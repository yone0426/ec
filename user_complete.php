<?php
  session_start();
  require 'db.php';

  if (!empty($_POST['submit'])){  
    $users = $db->prepare('SELECT * FROM users WHERE email=?');
    $users->execute(array($_SESSION['email']));
    $user = $users->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
    $_SESSION['user']['id'] = $user['id'];
    header('Location: product_list.php');
    exit;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>User登録</title>
</head>

<body>
  <h1>登録完了しました</h1><br>
  <form action="" method="post"><br>
  【名前】<br>
    <?php echo $_SESSION["name"]; ?><br>
  【メールアドレス】<br>
    <?php echo $_SESSION["email"]; ?><br>
  【住所】<br>
    <?php echo $_SESSION["address"]; ?><br>
  【パスワード】<br>
    <?php echo $_SESSION["password"]; ?><br><br>
    <input type="submit" name='submit' value='商品一覧ページへ'>
  </form>
</body>
</html>
