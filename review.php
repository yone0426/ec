<?php
  session_start();
  require('db.php');
  require('deny.php');
  if (!empty($_POST['submit'])) {
      if (empty($_POST['name'])) {
        $error_name = 'ニックネームを入力してください';
      }
      if (empty($_POST['review'])) {
        $error_review = 'レビューを入力してください';
      }
      if (empty($error_name) && empty($error_review)) {
        $statement = $db->prepare('INSERT INTO reviews SET name=?, review=?, product_id=?,user_id=? ');
        $statement->execute(array(
          $_POST['name'],
          $_POST['review'],
          $_REQUEST['id'],
          $_SESSION['user']['id']
        ));
        header("Location: product_detail.php?id=" .$_REQUEST['id'] );
        exit;
      }
  }
  if (!empty($_POST['return'])) {
    header("Location: product_detail.php?id=" .$_REQUEST['id'] );
    exit();
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>review登録</title>
</head>

<body>
  <h1>口コミ登録画面</h1><br>
  <form action="" method="post"><br>
    <br>ニックネーム<br>
    <input type="text" maxlength='50' id='name' name="name" value="<?php if(isset($_POST['name'])) print($_POST['name']) ?>" >
    <?php if(isset($error_name)): ?>
     <?php echo $error_name ?><br>
    <?php endif; ?>
    <br>レビュー<br>
    <textarea type="text" maxlength='140' name='review'><?php if(isset($_POST['review'])) print($_POST['review']) ?></textarea> 
    <?php if(isset($error_review)): ?>
      <?php echo $error_review ?>
    <?php endif;?>  <br>
    <br>
    <input type="submit" name='submit' value='レビュー登録'>
  </form>
  <form action="" method="post">
    <br><input type="submit" name="return" value="戻る">
  </form>
</body>
</html>
