<?php 
  if (empty($_SESSION['user']['id'])) {
    header('Location: login.php');
    exit();
  }
?>