<?php
  session_start();
  if (!empty($_POST['submit'])){
    $_SESSION = array();
    $_SESSION['user']['id'] = 100; 
    header('Location: product_list.php');
    exit;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品登録</title>
</head>

<body>
  <h1>商品登録完了しました</h1><br>
  <form action="" method="post"><br>
  【名前】<br>
    <?php echo $_SESSION["name"]; ?><br>
  【値段】<br>
    <?php echo $_SESSION["price"]; ?><br>
  【写真】<br>
    <?php echo $_SESSION["image"]; ?><br>
  【紹介文】<br>
    <?php echo $_SESSION["introduction"]; ?><br><br>
    <input type="submit" name='submit' value='商品一覧ページへ'>
  </form>
</body>
</html>

