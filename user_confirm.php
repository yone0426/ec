<?php
  session_start();
  require('db.php');
if (!empty($_POST['submit'])) { 
  header('Location: user_register.php');
  exit;
}

if (!empty($_POST['complete'])) {
  $statement = $db->prepare('INSERT INTO users SET name=?, email=?, password=?, address=? ');
  $statement->execute(array(
  $_SESSION['name'],
  $_SESSION['email'],
  password_hash($_SESSION['password'], PASSWORD_DEFAULT),
  $_SESSION['address'],
  ));
  header('Location: user_complete.php');
  exit();   
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>User登録</title>
</head>

<body>
  <h1>お客様確認画面</h1><br>
  <form action="" method="post"><br>
  【名前】<br>
    <?php echo $_SESSION["name"]; ?><br><br>
  【メールアドレス】<br>
    <?php echo $_SESSION["email"]; ?><br><br>
  【住所】<br>
    <?php echo $_SESSION["address"]; ?><br>
  【パスワード】<br>
    <?php echo $_SESSION["password"]; ?><br><br>
    <input type="submit" name='complete' value='登録'>
  </form>
  <form action="" method="post"><br>
    <input type="submit" name='submit' value='戻る'>
  </form>
</body>
</html>
