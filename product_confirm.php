<?php
  session_start();
  require('db.php');
  if (!empty($_POST['submit'])) { 
    header('Location: product_register.php');
    exit;
  }
 
  if (!empty($_POST['complete'])) {
    $statement = $db->prepare('INSERT INTO products SET name=?, price=?, image=?, introduction=? ');
	  $statement->execute(array(
		$_SESSION['name'],
		$_SESSION['price'],
		$_SESSION['image'],
		$_SESSION['introduction'],
	  ));
	  header('Location: product_complete.php');
	  exit();   
  }
?>
  
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品登録</title>
</head>

<body>
  <h1>商品登録確認画面</h1><br>
  <form action="" method="post"><br>
  【名前】<br>
    <?php echo $_SESSION["name"]; ?><br><br>
  【値段】<br>
    <?php echo $_SESSION["price"]; ?><br><br>
  【写真】<br>
    <img src="picture/<?php print($_SESSION['image']) ?>" width='100' height='100'><br>
  【紹介文】<br>
    <?php echo $_SESSION["introduction"]; ?><br><br>
    <input type="submit" name='complete' value='登録'>
  </form>

  <form action="" method="post"><br>
    <input type="submit" name='submit' value='戻る'>
  </form>
</body>
</html>
