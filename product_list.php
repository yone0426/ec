<?php
  session_start();
  require('db.php');
  require('deny.php');
  $products = $db->query('SELECT * FROM products');

  $items = $db->prepare('SELECT MAX(id) as id FROM products');
  $items->execute();
  $item = $items->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);

  $latests = $db->prepare('SELECT * FROM products WHERE id=?');
  $latests->execute(array($item['id']));
  $latest = $latests->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);

  if (!empty($_POST['logout'])) {
    $_SESSION = array();
    header('Location: login.php');
    exit();
  }
  $message = "";
  if (!empty($_POST['buy'])) {
    if (empty($_SESSION['cart'])){
      $message = 'カートの中は空です。';
    } else {
      header('Location: cart.php');
      exit;
    }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>一覧</title>
</head>

<body>
ー最新商品ー
<table border="2">
    <tr>
      <th>名前</th><td><?php print($latest['name']) ?></td>
    </tr>
    <tr>
      <th>値段</th><td><?php print($latest['price'])?></td>
    </tr>
    <tr>
      <th>写真</th><td><img src="picture/<?php print($latest['image'])?>" width="48" height="48" alt="<?php print($latest['name'])?>" /></td>
    </tr>
    <tr>
      <th>紹介文</th><td> <?php print($latest['introduction'])?></td>
    </tr>
    <tr>
      <th>詳細ページへ</th><td><a href="product_detail.php?id=<?php print($latest['id']) ?>">詳細</a></td>
    </tr>
  </table>

<br>〜商品一覧リスト〜<br>
  <table border="1">
    <thead>
      <tr>
        <th>商品ID</th>
        <th>商品名</th>
        <th>値段</th>
        <th>写真</th>
        <th>紹介文</th>
        <th>商品詳細画面へ</th>
      </tr>
    </thead>
    <?php foreach ($products as $product): ?>
    <tbody>
      <tr>
      <div>  
        <td><?php print($product['id']) ?></td>
        <td><?php print($product['name']) ?></td>
        <td><?php print($product['price']) ?></td>
        <td><img src="picture/<?php print($product['image']) ?>" width="80" height="80" alt="<?php print($product['name']) ?>" /></td>
        <td><?php print($product['introduction'])?></td>
        <td><a href="product_detail.php?id=<?php print($product['id']) ?>">詳細</a></td>
      </div>                      
      </tr>
    </tbody>
    <?php endforeach; ?> 
  </table>  

  <form action="" method="post"><br>
    <input type="submit" name='logout' value='ログアウト'>
  </form>
  <form action="ranking.php" method="post"><br>
    <input type="submit" name='ranking' value='ランキングページへ'>
  </form>
  <form action="" method="post"><br>
    <input type="submit" name='buy' value='カートの中身を確認'>
    <?php echo $message ?>
  </form>
</body>
</html>
