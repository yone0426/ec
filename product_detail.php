<?php
  session_start();
  require('db.php');
  require('deny.php');
  
  $id = $_REQUEST['id'];
  $products = $db->prepare('SELECT * FROM products WHERE id=?');
  $products->execute(array($id));
  $product = $products->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);

  $reviews = $db->prepare('SELECT * FROM reviews WHERE product_id=? ORDER BY id DESC' );
  $reviews->execute(array($id));
  $review = $reviews->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
  
  if (!empty($_POST['cart'])) {
    if (empty($_POST['amount'])) {
      $error_amount = '数量を入力してください';
    } else {
      $amount = $_POST['amount'];
      if(empty($_SESSION['cart'])) {
        $_SESSION['cart'][] = array('product_id'=>$id,'amount'=>$_POST['amount']);
        header('Location: cart.php');
        exit();
      } 
      if (isset($_SESSION['cart'])) {
        foreach ($_SESSION['cart'] as $key=>$value) {
          $check = $_SESSION['cart'][$key]['product_id'];
          if ($id === $check) {
            $_SESSION['cart'][$key]['amount'] += $amount;
            header('Location: cart.php');
            exit();
          } 
        }
        $_SESSION['cart'][] = array('product_id'=>$id,'amount'=>$_POST['amount']);
      }
      header('Location: cart.php');
      exit();
    }
  }

  if (!empty($_POST['delete'])){
    $statement = $db->prepare('DELETE FROM reviews WHERE id=?');
    $statement->execute(array(
      $_POST['review_id']
    ));
    header("Location: product_detail.php?id=" .$_REQUEST['id'] );
    exit();
  }

  $happys = $db->query('SELECT * FROM happy');
  if (!empty($_POST['like'])){
    foreach ($happys as $key=>$value) {
      if($value['user_id'] == $_SESSION['user']['id'] && $id == $value['product_id']){
        $delete = $db->prepare('DELETE FROM happy WHERE user_id=? AND product_id=?');
        $delete->execute(array($_SESSION['user']['id'],$id));
        header("Location: product_detail.php?id=" .$_REQUEST['id'] );
        exit();
      }  
    }
    $statement = $db->prepare('INSERT INTO happy SET user_id=?, product_id=?');
    $statement->execute(array(
      $_SESSION['user']['id'],
      $id,
    ));
    header("Location: product_detail.php?id=" .$_REQUEST['id'] );
    exit();
  }

  $totals = $db->prepare('SELECT count(id) FROM happy WHERE product_id=?');
  $totals->execute(array($id));
  $total = $totals->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
  $error_review ="";
  // var_dump($review);
  // exit;
  if (empty($review))  {
    $error_review = "口コミはまだありません";
  }
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品詳細</title>
</head>

<body>
  <br>〜商品詳細〜<br>     
  <table border="2">
    <tr>
      <th>商品ID</th><td><?php print($product['id'])?></td>
    </tr>
    <tr>
      <th>名前</th><td><?php print($product['name']) ?></td>
    </tr>
    <tr>
      <th>値段</th><td><?php print($product['price'])?></td>
    </tr>
    <tr>
      <th>写真</th><td><img src="picture/<?php print($product['image'])?>" width="48" height="48" alt="<?php print($product['name'])?>" /></td>
    </tr>
    <tr>
      <th>紹介文</th><td> <?php print($product['introduction'])?></td>
    </tr>
  </table><br>
  <br><a href="review.php?id=<?php print($product['id']) ?>">口コミ投稿</a><br>

  <form action="" method="post">
    <br><input type="submit" name="like" value="いいね"><?php echo " ".$total['count(id)'] ?>
  </form>

  <?php if(empty($review)) echo $error_review ?> 
  <br>〜口コミ〜<br>  
  <table border="2">
    <thead>
    <tbody>
  <tr>
    <th>ニックネーム</th> <th>レビュー</th> <th>削除</th>
  </tr>
    </thead>
  <?php foreach ($reviews as $review): ?>   
  <tr>
    <td><?php print($review['name'])?></td> 
    <td><?php print($review['review']) ?></td>
    <td>
    <form action="" method="post">
      <input type="hidden" name="review_id" value="<?php print($review['id']) ?>" >
      <?php if($_SESSION['user']['id'] == $review['user_id']): ?>
        <input type="submit" name='delete' value='削除'>
      <?php endif; ?>
    </form>
    </td>
  </tr>
  <tbody>
  <?php endforeach ?>
  </table>

  <form action="product_list.php" method="post">
    <br><input type="submit" name="return" value="商品一覧へ">
  </form>
  <form action="" method="post"><br>
    個数<input type="number" name='amount' min="1" value="">
    <?php if(isset($error_amount)): ?>
    <?php echo $error_amount ?>
    <?php endif;?><br>
    <br><input type="submit" name="cart" value="カートに追加">
  </form>
</body>
</html>
