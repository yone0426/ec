<?php
  session_start();
  if (!empty($_POST['submit'])) { 
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['address'] = $_POST['addr11'];
    $_SESSION['zip_code'] = $_POST['zip_code'];
    $_SESSION['email'] = $_POST['email'];
    $_SESSION['password'] = $_POST['password'];
    if (empty($_POST['name'])) {
      $error_name = '名前を入力してください';
    }
    if (empty($_POST['email'])) {
      $error_email = 'メールアドレスを入力してください';
    }
    if (empty($_POST['addr11'])) {
      $error_address = '住所を入力してください';
    }
    if (empty($_POST['zip_code'])) {
      $error_zip_code = '郵便番号を入力してください';
    }
    if (empty($_POST['password'])) {
      $error_password = 'パスワードを入力してください';
    }
    if (empty($error_name) && empty($error_email) && empty($error_address) && empty($error_zip_code) && empty($error_password)) {
      header('Location: user_confirm.php');
      exit;
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <title>User登録</title>
</head>

<body>
  <h1>お客様登録画面</h1><br>
  <form action="" method="post"><br>
    <br>名前<br>
    <input type="text" id='name' name="name" value="<?php if(isset($_SESSION['name'])) print($_SESSION['name']) ?>" >
    <?php if(isset($error_name)) echo $error_name ?><br>

    メールアドレス<br>
    <input type="email" name='email' value="<?php if(isset($_SESSION['email'])) print($_SESSION['email']) ?>" >
    <?php if(isset($error_email)) echo $error_email ?><br>

    郵便番号<br>
    <input type="text" name="zip_code" value="<?php if(isset($_SESSION['zip_code'])) print($_SESSION['zip_code']) ?>" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','addr11','addr11');">
    <?php if(isset($error_zip_code)) echo $error_zip_code ?><br>

    <br>住所<br>
    <input type="text" name="addr11" value="<?php if(isset($_SESSION['address'])) print($_SESSION['address']) ?>" size="60">
    <?php if(isset($error_address)) echo $error_address ?><br>

    <br>パスワード<br>
    <input type="text" name='password' value="<?php if(isset($_SESSION['password'])) print($_SESSION['password']) ?>" >
    <?php if(isset($error_password)) echo $error_password ?><br>
    <br>
    <input type="submit" name='submit' value='確認画面へ'>
  </form>
</body>
</html>
    
