<?php
session_start();
require('db.php');
if (!empty($_POST['submit'])) {
  $login = $db->prepare('SELECT * FROM users WHERE email=?');
  $login->execute(array(
    $_POST['email'],
  ));
  $success = $login->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
  if (password_verify($_POST['password'], $success['password'])) {
    $_SESSION['user']['id'] = $success['id'];
    header('Location: product_list.php');
    exit();
  }  else {
    $error = '正しいアドレスとパスワードを入力して';
  }
}
if(isset($_POST['return'])) {
  header('Location: user_register.php');
  exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>login</title>
</head>

<body>
  <h1>ログインフォーム</h1>
  <form action="" method="post">
  <?php if(!empty($error)) echo $error ?>
    <br>メールアドレス<br>
    <input type="email" name='email' value="<?php if (!empty($_POST)) print($_POST['email']) ?>"><br>
    <br>パスワード<br>
    <input type='text' name="password" value=""><br><br>
    <input type="submit" name='submit' value='ログイン'>
    <input type="submit" name='return' value='新規登録へ'>
  </form>
</body>
</html>
