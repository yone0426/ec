<?php
  session_start();
  require('db.php');
  require('deny.php');
  if (empty($_SESSION['cart'])) {
    header('Location: product_list.php');
    exit;
  }

  $amount_list = array_column( $_SESSION['cart'], 'amount' );
  $id = $_SESSION['user']['id'];

  $products = $db->prepare('SELECT * FROM users WHERE id=?');
  $products->execute(array($id));
  $product = $products->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
  $address = $product['address'];

  foreach ($_SESSION['cart'] as $key=>$value) {
    $products = $db->prepare('SELECT * FROM products WHERE id=?');
    $products->execute(array($value['product_id']));
    $product = $products->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
    $cart_list[] = $product;
    $cart_list[$key]['amount'] = $value['amount'];
  }  
  $all_price = 0;
  foreach ($cart_list as $key=>$value){
  $all_price += $value['amount'] * $value['price'];
  }

  if (!empty($_POST['decide'])) {
    $_SESSION['address'] = $_POST['addr11'];
    $_SESSION['zip11'] = $_POST['zip11'];
    if (empty($_POST['addr11'])) {
      $error_address = '住所を入力してください';
    }
    if (empty($_POST['zip11'])) {
      $error_zip11 = '郵便番号を入力してください';
    }
    if (empty($error_address) && empty($error_zip11)) {
      try {
        $db = new PDO('mysql:dbname=EC; host=localhost; charset=utf8','root','');
        $db->beginTransaction();
        $statement = $db->prepare('INSERT INTO orders SET users_id=?, address=?, all_price=?');
        $statement->execute(array(
          $_SESSION['user']['id'],
          $_SESSION['address'],
          $all_price,
        ));
        $id_last = $db->lastInsertId('id');
        foreach ($_SESSION['cart'] as $key=>$value){
          $statement = $db->prepare('INSERT INTO order_detail SET order_id=?, product_id=?, amount=?');
          $statement->execute(array(
            $id_last,
            $_SESSION['cart'][$key]['product_id'],
            $_SESSION['cart'][$key]['amount'],
          ));
        } 
        $db->commit();
        unset($_SESSION['cart']);
      } catch (PDOException $e) {
        $db->rollBack();
        print('トランザクションエラーです:' . $e->getMessage());
        exit;
      }
      require("mail_test.php");
      header('Location: order_complete.php');
      exit();   
    }
  }  
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <title>一覧</title>
</head>
<br>
注文確認画面<br>
<body><br><br>【カートの一覧】<br><br>
  <table border="1">
      <thead>
        <tr>
          <th>商品ID</th>
          <th>商品名</th>
          <th>値段</th>
          <th>写真</th>
          <th>紹介文</th>
          <th>数量</th>
          <th>合計金額</th>
          <th>削除</th>
        </tr>
      </thead>
      <?php foreach ($cart_list as $key=>$value): ?>
      <tbody>
        <tr>
        <div>  
          <td><?php print($value['id'])?></td>
          <td><?php print($value['name']) ?></td>
          <td><?php print($value['price']) ?></td>
          <td><img src="picture/<?php print($value['image']) ?>" width="48" height="48" alt="<?php print($value['name']) ?>" /></td>
          <td><?php print($value['introduction']) ?></td>         
          <td><?php print($value['amount']) ?></td>
          <td><?php print($value['amount'] * $value['price']); ?></td>
          <td><a href="delete.php?id=<?php print($value['id']) ?>">削除</a></td>
        </div>                      
        </tr>
      </tbody>
      <?php endforeach; ?> 
    </table>
  <form action="" method="post"><br>
    <table border="2">
    <tr>
      <th>郵便番号</th><td><input type="text" name="zip11" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','addr11','addr11');">
    <?php if(isset($error_zip11)) echo $error_zip11 ?></td>
    </tr>
    <tr>
      <th>住所</th><td><input type="text" name="addr11" size="60">
    <?php if(isset($error_address)) echo $error_address ?></td>
    </tr>
    <tr>
      <th>支払い方法</th><td>銀行振込</td>
    </tr>
    <tr>
      <th>カート全体金額</th><td><?php echo $all_price ?>円</td>
    </tr>
  </table>

    <br><input type="submit" name='decide' value='確定'>
  </form>
  <form action="cart.php" method="post"><br>
    <input type="submit" name='back' value='戻る'>
  </form>
  
</body>
</html>
<br>
