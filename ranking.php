<?php
  session_start();
  require('db.php');
  require('deny.php');
  $n = 1;
  $products = $db->query('SELECT product_id,SUM(amount),name,image,price,introduction FROM order_detail,products WHERE products.id=order_detail.product_id GROUP BY product_id ORDER BY SUM(amount) DESC LIMIT 0, 5;');
  // $products = $db->query('SELECT product_id,SUM(amount)FROM order_detail,products WHERE products.id=order_detail.product_id GROUP BY product_id ORDER BY SUM(amount) DESC LIMIT 0, 5');
  // var_dump($products);
  // exit;
  // foreach ($products as $value){
  //   $id[] = $value['product_id'];
  // }
  // foreach($id as $product_id ){
  //   $items = $db->query('SELECT * FROM products WHERE id=?');
  //   $items->execute(array($product_id));
  // }

  ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>一覧</title>
</head>

<body>
<br>〜売れ筋ランキング〜<br>
  <table border="1">
    <thead>
      <tr>
        <th>ランキング順位</th>
        <th>商品名</th>
        <th>値段</th>
        <th>写真</th>
        <th>紹介文</th>
        <th>商品詳細画面へ</th>
      </tr>
    </thead>
    <?php foreach ($products as $product): ?>
    <tbody>
      <tr>
      <div>  
        <td><?php print($n) ?></td>
        <?php $n++ ?>
        <td><?php print($product['name'])?></td>
        <td><?php print($product['price']) ?></td>
        <td><img src="picture/<?php print($product['image']) ?>" width="110" height="110" alt="<?php print($product['name']) ?>" /></td>
        <td><?php print($product['introduction'])?></td>
        <td><a href="product_detail.php?id=<?php print($product['product_id']) ?>">詳細</a></td>
      </div>                      
      </tr>
    </tbody>
    <?php endforeach; ?> 
  </table>  

  <form action="product_list.php" method="post"><br>
    <input type="submit" name='return' value='戻る'>
  </form>
</body>
</html>
