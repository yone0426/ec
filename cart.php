<?php
  session_start();
  require('db.php');
  require('deny.php');
  $error = "";
  if (empty($_SESSION['cart'])) {
    header('Location: product_list.php');
    exit;
  }

  $_SESSION['cart'] = array_merge($_SESSION['cart']);
  
  if (!empty($_POST['cart'])) {
    unset($_SESSION['cart']);
    header('Location: product_list.php');
    exit();
  }

  if (!empty($_POST['submit'])) {
    if (empty($_POST['add'])) {
      $error = '増減する場合は数量を入力してください';
    } else {
      $product_id = $_POST['id'];
      $add = $_POST['add'];
      foreach ($_SESSION['cart'] as $key=>$value) {
        $product_id_SESSION = $_SESSION['cart'][$key]['product_id'];
        if ($product_id === $product_id_SESSION){
          $_SESSION['cart'][$key]['amount'] += $add;
          if($_SESSION['cart'][$key]['amount'] <= 0) { 
            unset($_SESSION['cart'][$key]);
            header('Location: cart.php');
          exit();
          }
        }
      }
    }
  }

  foreach ($_SESSION['cart'] as $key=>$value) {
    $products = $db->prepare('SELECT * FROM products WHERE id=?');
    $products->execute(array($value['product_id']));
    $product = $products->fetch(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
    $cart_list[] = $product;
    $cart_list[$key]['amount'] = $value['amount'];
  }
  $all_price = 0;
  foreach ($cart_list as $key=>$value){
  $all_price += $value['amount'] * $value['price'];
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>一覧</title>
</head>

<body><br><br>【カートの一覧】<br><br>
  <table border="1">
      <thead>
        <tr>
          <th>商品ID</th>
          <th>商品名</th>
          <th>値段</th>
          <th>写真</th>
          <th>紹介文</th>
          <th>数量</th>
          <th>数量の増減</th>
          <th>合計金額</th>
          <th>削除</th>
        </tr>
      </thead>
      <?php foreach ($cart_list as $key=>$value): ?>
      <tbody>
        <tr>
        <div>  
          <td><?php print($value['id']) ?></td>
          <td><?php print($value['name']) ?></td>
          <td><?php print($value['price']) ?></td>
          <td><img src="picture/<?php print($value['image']) ?>" width="100" height="100" alt="<?php print($value['name'])?>"></td>
          <td><?php print($value['introduction'])?></td>         
          <td><?php print($value['amount'])?>
          <td>
          <form action="" method="post">
            <input type="hidden" name='id'  value="<?php print($value['id']) ?>">
            <input type="number" min="<?php print($value['amount'] * -1) ?>" name='add'>
            <input type="submit" name='submit' value='数量を変更する'>
          </form>
          </td>
          <td><?php print($value['amount'] * $value['price']) ?></td>
          <td><a href="delete.php?id=<?php print($value['id']) ?>">削除</a></td>
        </div>                      
        </tr>
      </tbody>
      <?php endforeach; ?> 
    </table>
    <?php echo $error ?>
      <br>カート合計金額 : <?php echo $all_price ?>円
  <form action="order_confirm.php" method="post"><br>
    <input type="submit" name='cart' value='購入手続きに進む'>
  </form>
  <form action="product_list.php" method="post"><br>
    <input type="submit" name='list' value='商品一覧へ戻る'>
  </form>
  <form action="" method="post">
    <br><input type="submit" name="cart" value="カートを空にする">
  </form>

</body>
</html>
