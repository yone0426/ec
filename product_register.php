<?php
  session_start();
  $error_name = "";
  $error_price = "";
  $error_image = "";
  $error_introduction = "";
  if (!empty($_POST['submit'])) { 
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['price'] = $_POST['price'];
    $_SESSION['introduction'] = $_POST['introduction'];
    $image = $_FILES['image']['name'];
    move_uploaded_file($_FILES['image']['tmp_name'],'picture/' . $image);
    $_SESSION['image'] = $image;
    if (empty($_POST['name'])) {
      $error_name = '名前を入力してください';
    }
    if (empty($_POST['price'])) {
      $error_price = '値段を入力してください';
    }
    if (empty($_POST['image'])) {
      $error_image = '画像を入力してください';
    }
    if (empty($_POST['introduction'])) {
      $error_introduction = '紹介文を入力してください';
    }
    if (empty($error_name) && empty($error_price)  && empty($error_introduction)) {
      header('Location: product_confirm.php');
      exit;
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品登録</title>
</head>

<body>
  <h1>商品登録画面</h1><br>
  <form action="" method="post" enctype="multipart/form-data"><br>
    <br>名前<br>
    <input type="text" id='name' name="name" value="<?php if(isset($_SESSION['name'])) print($_SESSION['name']) ?>" ><?php echo $error_name ?><br>
    値段<br>
    <input type="number" name='price' min="1" value="<?php if(isset($_SESSION['price'])) print($_SESSION['price']) ?>" ><?php echo $error_price ?><br>
    写真<br>
    <input type="file" name='image' value="<?php if(isset($_SESSION['image'])) print($_SESSION['image']) ?>" ><?php echo $error_image ?><br>
    紹介文<br>
    <textarea type="text" name='introduction'><?php if(isset($_SESSION['introduction'])) print($_SESSION['introduction']) ?></textarea><?php echo $error_introduction ?><br>
    <br>
    <input type="submit" name='submit' value='商品出品確認画面へ'>
  </form>
</body>
</html>

